package shopping

import org.scalatest.FunSuite

class ShoppingCartTest extends FunSuite {

  val testShoppingCart = new ShoppingCart(ShoppingCart.pricelist)

  test("Checking out with no items should cost 0") {
    assert(testShoppingCart.checkout(Seq()) === 0.0)
  }

  test("Checking out with single Apple") {
    assert(testShoppingCart.checkout(Seq("Apple")) === 0.6)
  }

  test("Checking out with many items") {
    assert(testShoppingCart.checkout(Seq("Apple", "Apple", "Orange", "Apple")) === 2.05)
  }

  test("An unexpected item in the cart should be ignored") {
    assert(testShoppingCart.checkout(Seq("Pineapple", "Orange")) === 0.25)
  }

}
