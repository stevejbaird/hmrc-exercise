package shopping

import org.scalatest.FunSuite

class OffersTest extends FunSuite {

  val testShoppingCart = new ShoppingCartWithOffers(ShoppingCart.pricelist, Offers.offers)

  test("Checking out with two Apples the 2 for 1 offer should be applied") {
    val items: Seq[String] = Seq("Apple", "Apple")
    assert(testShoppingCart.checkout(items) === 0.6)
  }

  test("Checking out with three Oranges the 3 for 2 offer should be applied") {
    val items: Seq[String] = Seq.fill(3)("Orange")
    assert(testShoppingCart.checkout(items) === 0.5)
  }

  test("Promotion should should only be applied once where not enough to qualify for offer a second time") {
    val items: Seq[String] = Seq.fill(3)("Apple")
    assert(testShoppingCart.checkout(items) === 1.2)
  }

  test("Promotion should should be applied twice when twice the number of the qualifying amount") {
    val items: Seq[String] = Seq.fill(4)("Apple")
    assert(testShoppingCart.checkout(items) === 1.2)
  }
}
