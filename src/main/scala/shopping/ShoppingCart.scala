package shopping

object ShoppingCart {
  val pricelist = Map(
    "Apple" -> BigDecimal(0.6),
    "Orange" -> BigDecimal(0.25)
  ).withDefaultValue(BigDecimal(0.0))

  def main(args: Array[String]) = {
    println(new ShoppingCart().checkout(args))
  }
}

class ShoppingCart (val pricelist: Map[String, BigDecimal] = ShoppingCart.pricelist) {
  def checkout(items: Seq[String]) = items.map(pricelist).sum
}
