package shopping

import ShoppingCart.pricelist

object Offers {

  val offers = Seq(
    discount("Apple", 2, 1)_,
    discount("Orange", 3, 2)_
  )

  def discount(item: String, qualifyingQuantity: Int, chargedQuantity: Int)(items: Seq[String]) = {
    val actualQuantity = items.count(_ == item)
    val discountUnits = actualQuantity / qualifyingQuantity
    val discountPerUnit = (qualifyingQuantity - chargedQuantity) * pricelist(item)
    discountUnits * discountPerUnit
  }

  def main(args: Array[String]) = {
    println(new ShoppingCartWithOffers().checkout(args))
  }
}

class ShoppingCartWithOffers(pricelist: Map[String, BigDecimal] = ShoppingCart.pricelist,
                             val offers: Seq[Seq[String] => BigDecimal] = Offers.offers) extends ShoppingCart(pricelist) {

  override def checkout(items: Seq[String]): BigDecimal = applyOffers(items, super.checkout(items))

  private def applyOffers(items: Seq[String], total: BigDecimal) = {
    val discounts = offers.map(discountFn => discountFn(items))
    total - discounts.sum
  }
}
